# Retrospective Board

> **Note:** This repository contains boilerplate code for an exercise. You can download or fork the repository and add your implementation.

Retrospective board is a collaborative product that can be used by remote teams to conduct their retrospectives easily. Each user can place a post-it to the canvas with a text on it. Later, the post-it can be moved into another location on the canvas. 

## Prerequisites

You should have `clojure`, `node` and `yarn` installed in your machine.
- [Clojure](https://clojure.org/guides/install_clojure)
- [NodeJS](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/getting-started/install)

## Running the Project

Run the following commands in your terminal to install dependencies

First install the node dependencies

```
yarn
```

Execute the following to start the project

```
yarn watch
```

This will download necessary ClojureScript dependencies and start a web server at [http://localhost:3000](http://localhost:3000). Open the URL in your browser and the retrospective board should be visible.

> **Note:** This project uses [Shadow CLJS](https://shadow-cljs.github.io/docs/UsersGuide.html) 

## Project Structure

```
▾ public
  ▸ css             → Stylesheets
  ▸ js              → Generated Javascript files
  ▸ index.html      → HTML file for SPA
▾ src
  ▸ components      → UI components
  ▸ util            → Utilities. E.g. Konva, Y.js
  ▸ controller.cljs → Application logic
  ▸ app.cljs        → Main entry point
▸ test              → Test sources
▸ package.json      → NodeJS dependencies
▸ shadow-cljs.edn   → Shadow CLJS configuration
``` 

## Requirements
> You can change the existing code and the way the product looks and functions however you like. The current state of this project is only here to let you get going quickly.

Below are the functionalities that are expected from the *Restrospective Board* product. You can also implement the extras listed in the next section (or any other feature that you think that would be valuable for the product) once these requirements are in-place. 

- [x] The users should be able to place post-its to the retrospective board.
- [x] A text can be written inside each post-it.
- [x] Post-its on the board can be moved around easily by users.
- [x] Existing post-its can be removed from the board.
- [x] The users should be able to move around the board to see content that is not currently visible on the screen.
- [x] There should be a way to zoom in to and out from the board.
- [ ] The retrospective board should work collaboratively.

> **Note:** This boilerplate repository already contains necessary code for enabling the collaborative work via [Y.js](https://docs.yjs.dev/) using [WebRTC](https://webrtc.org/).

## Extras

- [ ] **Styling:** It's up to you how much do you want to style the project. It's always fun to use beautifully looking product.
- [ ] **Undo/Redo:** Wouldn’t it be nice to be able to take back a mistake? We don’t think it would be hard to achieve if you reached here.
- [ ] **Storage:** It is a shame if you accidentally hit refresh and loose your retrospective. It would be great if it’s saved locally.
- [x] **Multiple Selection:** It can be handy to be able to select multiple post-its at once and move them around together, instead of one by one.
