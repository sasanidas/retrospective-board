(ns com.scrintal.retrospective.app
  (:require [com.scrintal.retrospective.components.collaborative-canvas :as canvas]
            [com.scrintal.retrospective.components.join :as join]
            [com.scrintal.retrospective.controller :as controller]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(defonce state (r/atom {}))

(defn- on-join [connection-details]
  (swap! state assoc
         :connection-details connection-details))

(defn- app []
  [:div {:class "app"}
   (if (:connection-details @state)
     [canvas/render
      (:connection-details @state)
      controller/on-change-canvas
      controller/on-change-y-map]
     [join/render on-join])])

#_{:clj-kondo/ignore [:clojure-lsp/unused-public-var]}
(defn ^:dev/after-load start! []
  (rdom/render
   [app]
   (js/document.getElementById "root")))

#_{:clj-kondo/ignore [:clojure-lsp/unused-public-var]}
(defn init! []
  (start!))
