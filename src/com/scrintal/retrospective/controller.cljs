(ns com.scrintal.retrospective.controller
  (:require [com.scrintal.retrospective.util.konva :as konva]
            [com.scrintal.retrospective.util.yjs.core :as yjs]
            [com.scrintal.retrospective.util.yjs.interface.event :as y.event]
            [com.scrintal.retrospective.util.yjs.interface.map :as y.map]
            [com.scrintal.retrospective.util.yjs.interface.transaction :as y.transaction]))

(defn- update-element-to-y-map!
  "Converts element map into a Y.Map instance and inserts the
   new element with it's id to the 'elements' map in Y.Doc."
  [elements-y-map {:keys [id]
                   :as   element-data}]
  (let [new-element-y-map (yjs/map->y-map element-data)]
    (y.map/set! elements-y-map id new-element-y-map)))

(defn- remove-element-to-y-map! ""
  [elements-y-map {:keys [id]
                   :as element-data}]
  (y.map/delete! elements-y-map id))

(defn on-change-y-map
  "Change handler for Yjs collaboration. This function is triggered when the elements
   Y.Map or any of its children changed. You can use this to receive changes from
   other peers and draw those changes on the Konva stage."
  [^js stage {:keys [events transaction]}]
  ;; Check if the transaction is local or not. Ignore local transactions.
  (when-not (y.transaction/local? transaction)
    (doseq [event events]
      (doseq [element-id (y.event/changed-keys event)]
        (let [element-data (y.map/get (y.event/target event) element-id)
              clj-data (and element-data (y.map/->clj element-data))]
          (if clj-data
            (konva/upsert-element! stage element-id clj-data)
            (konva/destroy!
             (konva/find-one-by-id stage element-id))))))))

(defn on-change-canvas
  "Change handler for Konva canvas. This function is triggered when anything changes on
   canvas. When a new event is emited from Konva via emit-event! function, this function
   gets called."
  [y-map ^js _stage {:keys [type data]}]
  (case type
    :update-post-it
    (update-element-to-y-map! y-map data)
    :remove-post-it 
    (remove-element-to-y-map! y-map data)))
