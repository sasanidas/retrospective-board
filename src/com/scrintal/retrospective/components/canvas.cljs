(ns com.scrintal.retrospective.components.canvas
  (:require [com.scrintal.retrospective.util.konva :as konva]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(defn- on-resize-canvas [^js stage ^js events]
  (let [^js/Object first-event (first events)
        width       (.. first-event -contentRect -width)
        height      (.. first-event -contentRect -height)]
    (konva/set-size! stage {:width  width
                            :height height})))

(defn create-resize-observer-fn [stage-atom]
  #(on-resize-canvas @stage-atom %))

(defn render [{:keys [on-change
                      on-stage-ready
                      on-stage-destroyed]}]
  (let [stage-atom (atom nil)
        ^js observer   (js/window.ResizeObserver. (create-resize-observer-fn stage-atom))]
    (r/create-class
     {:display-name           "Canvas Wrapper"
      :component-did-mount    (fn [this]
                                (let [^js dom-node (rdom/dom-node this)
                                      height   (.-clientHeight dom-node)
                                      width    (.-clientWidth dom-node)
                                      ^js stage    (konva/init! dom-node on-change width height)]
                                  (reset! stage-atom stage)
                                  (.observe observer dom-node)
                                  (when on-stage-ready
                                    (on-stage-ready stage))))
      :component-will-unmount (fn [this]
                                (let [^js dom-node (rdom/dom-node this)]
                                  (konva/destroy! @stage-atom)
                                  (reset! stage-atom nil)
                                  (.unobserve observer dom-node)
                                  (when on-stage-destroyed
                                    (on-stage-destroyed))))
      :reagent-render         (fn []
                                [:div {:class "canvas-wrapper"}])})))
