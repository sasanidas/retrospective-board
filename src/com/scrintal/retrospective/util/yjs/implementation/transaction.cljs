(ns com.scrintal.retrospective.util.yjs.implementation.transaction
  (:require ["yjs" :refer [UndoManager]]
            [com.scrintal.retrospective.util.yjs.interface.transaction :as y.transaction]
            [com.scrintal.retrospective.util.yjs.implementation.undo-manager :as undo-manager-impl]))

(defrecord YTransactionImpl [^js y-transaction]
  y.transaction/YTransaction
  (transaction* [_]
    y-transaction)

  (origin [_]
    (let [^js origin (.-origin y-transaction)]
      (if (instance? UndoManager origin)
        (undo-manager-impl/->YUndoManagerImpl origin)
        origin)))

  (initial? [_]
    (zero? (.. y-transaction -beforeState -size)))

  (local? [_]
    (true? (.-local y-transaction))))
