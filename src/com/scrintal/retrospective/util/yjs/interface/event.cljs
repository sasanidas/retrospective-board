(ns com.scrintal.retrospective.util.yjs.interface.event)

(defprotocol YEvent
  "Represents a Y.js event and provides access methods
   to underlying implementation.
   
   Refer to:
     https://docs.yjs.dev/api/shared-types/y.map#observing-changes-y.mapevent
     https://docs.yjs.dev/api/y.event"

  (event* [y-event]
    "Returns the underlying Y.Doc instance.")

  (target [y-event]
    "The shared type that this event was created on. This event
     describes the changes on target.")

  (parent [y-event]
    "The shared type that is the parent of the shared type that 
     this event was created on.")

  (root? [y-event]
    "Returns true if the shared type that this event was created on
     is the root shared type.")

  (changed-keys [y-event]
    "Returns a set of changed keys that changed.")

  (changes [y-event callback-fn]
    "Computes the changes and calls the callback-fn for each change.
     callback-fn should be a single arity function where the argument
     is a change. A change is a map of :action, :key and optional :old-value
     keys where :action could be one of :add, :update or :delete, the :key 
     will be the key in the Y.Map. If the change is either :update or :delete,
     :old-value will include the previous value of the given key.")

  (path [y-event]
    "Computes the path from the Y.Doc to the changed type.
     Returns the path vector."))
